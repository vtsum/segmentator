import cv2

def getIntersectionOverUnion(bbox1, bbox2):
    bbox1_x1 = bbox1[0]
    bbox1_y1 = bbox1[1]
    bbox1_x2 = bbox1[2]
    bbox1_y2 = bbox1[3]

    bbox2_x1 = bbox2[0]
    bbox2_y1 = bbox2[1]
    bbox2_x2 = bbox2[2]
    bbox2_y2 = bbox2[3]

    x1 = max(bbox1_x1, bbox2_x1)
    y1 = max(bbox1_y1, bbox2_y1)
    x2 = min(bbox1_x2, bbox2_x2)
    y2 = min(bbox1_y2, bbox2_y2)
    
    if x2 < x1 or y2 < y1:
        return 0.0, (x1, y1, x2, y2)

    intersection = (x2 - x1) * (y2 - y1)

    areaA = (bbox1_x2 - bbox1_x1) * (bbox1_y2 - bbox1_y1)
    areaB = (bbox2_x2 - bbox2_x1) * (bbox2_y2 - bbox2_y1)
    
    union = areaA + areaB - intersection
    
    return intersection / union, (x1, y1, x2, y2)

def imageSegmentation(inputImage, nextFrame, imageSavePath="", cropImage=True, showContour=False, contourOffset = 50):
    image = originalImage = cv2.imread(inputImage)
    compareWithImage = cv2.imread(nextFrame)

    image = cv2.absdiff(image, compareWithImage)

    image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    image = cv2.convertScaleAbs(image, alpha=4)
    image = cv2.medianBlur(image, 31)

    _, image = cv2.threshold(image, 40, 255, cv2.THRESH_BINARY)

    contours, _ = cv2.findContours(image, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
    biggestContour = max(contours, key = cv2.contourArea)    
    x, y, w, h = cv2.boundingRect(biggestContour)

    xStart = x - contourOffset
    yStart = y - contourOffset
    xEnd = x + w + contourOffset
    yEnd = y + h + contourOffset

    image = cv2.cvtColor(image, cv2.COLOR_GRAY2BGR)
    cv2.rectangle(image, (xStart, yStart), (xEnd, yEnd), (255, 255, 255), -1)

    image = cv2.bitwise_and(originalImage, image)

    if cropImage:
        image = image[yStart:yEnd, xStart:xEnd]

    if showContour:
        cv2.rectangle(image, (xStart, yStart), (xEnd, yEnd), (255, 255, 255), 4)
        image = cv2.drawContours(image, contours, -1, (255, 0, 0), 8)

    if imageSavePath != "":
        cv2.imwrite(imageSavePath, image)

    return (xStart, yStart, xEnd, yEnd), image
