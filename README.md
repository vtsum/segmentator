# Segmentator

Foreground segmentator based on a principle of two frames difference. Frames with a static background and moving object can be segmentated to remove a background.

## Installation
```bash
pip3 install "git+https://gitlab.com/vtsum/segmentator.git"
```

## Usage
```python
from segmentator import imageSegmentation

bbox, image = imageSegmentation("./input/1/0.png", "./input/1/1.png")
```

### Parameters
`inputImage` — Path to input image

`nextFrame` — Path to next frame of input image

`imageSavePath` — Path to save output image

`cropImage` — __True__ | __False__ (default: __True__)

`showContour` — __True__ | __False__ (default: __False__)

`contourOffset` — __Number__ (default: __50__)

### Returns
`bbox` — tuple of bbox coords

`image` — OpenCV image

## Test cases

### Clone repo
```bash
git clone "git+https://gitlab.com/vtsum/segmentator.git"
```

### Run test case
```bash
cd segmentator
python3 test/test_runner.py --test_case "./test_suite/test_case0.json"
```

### Args help
```bash
python3 test/test_runner.py --help
```

### Example of test case
You can find example in `./test/test_suite/`