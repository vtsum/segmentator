from setuptools import setup, find_packages

setup(
    name='segmentator',
    version='0.1.0',
    packages=find_packages(include=['segmentator', 'segmentator.*']),
    install_requires=['opencv-python==4.8.0.74', 'matplotlib==3.7.2'],
)