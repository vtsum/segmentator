import argparse
import json
import os
import cv2
import matplotlib.pyplot as plt
from segmentator import imageSegmentation, getIntersectionOverUnion

parser = argparse.ArgumentParser(description='Segmentator test runner')
parser.add_argument('-t', '--test_case', type=str, required=True, help='Relative path to JSON file with test case. Example: "./path/to/testCase.json"', metavar='')
parser.add_argument('-v', '--show_visual', type=str, help='Show visual bbox. Options: "Always" | "OnTestFailed"', metavar='')
args = parser.parse_args()

jsonPath = os.path.join(os.path.dirname(__file__), args.test_case)
testSuitePath = os.path.join(os.path.dirname(__file__), './test_suite/')

with open(jsonPath, 'r') as f:
    testCase = json.load(f)

inputImagePath = os.path.join(testSuitePath, testCase['input_image'])
nextFramePath = os.path.join(testSuitePath, testCase['next_frame'])

bbox, image = imageSegmentation(inputImagePath, nextFramePath, cropImage=False, showContour=True)

expectedBbox = tuple(testCase['expected_bbox'])
toleranceIoU = testCase['tolerance_iou_percentage'] / 100

IoU, (x1, y1, x2, y2) = getIntersectionOverUnion(bbox, expectedBbox)

testPassed = IoU >= 1 - toleranceIoU

print('Expected bbox:', expectedBbox)
print('Segmentation bbox:', bbox)
print("IoU:", IoU)

if (testPassed):
    print("TEST PASSED")
else:
    print("TEST FAILED")
    
if args.show_visual == 'Always' or (args.show_visual == 'OnTestFailed' and not testPassed):
    cv2.rectangle(image, (bbox[0], bbox[1]), (bbox[2], bbox[3]), (0, 0, 255), 4)
    cv2.rectangle(image, (expectedBbox[0], expectedBbox[1]), (expectedBbox[2], expectedBbox[3]), (0, 255, 0), 4)

    plt.imshow(cv2.cvtColor(image, cv2.COLOR_BGR2RGB))
    plt.show()
